# Factory Design Pattern with Object Pool for Unity

I assume that you are looking for some information about how to implement object pooling in your Unity project.

This repository contains example how you can implement it! In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/04/01/how-to-implement-object-pooling-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement and use Object Pooling.

If you want to see that implementation of it, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/factory-with-object-pool/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

You have just learned about Object Pooling in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com


